const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('../helpers');
const { getProfile, removeUser, changePass } = require('../controllers/userController');

router.get('/me', asyncWrapper(getProfile));
router.patch('/me', asyncWrapper(changePass));
router.delete('/me', asyncWrapper(removeUser));

module.exports = router;