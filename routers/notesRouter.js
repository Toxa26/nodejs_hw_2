const express = require('express');
const router = express.Router();

const { Note } = require('../models/noteModel');
const { asyncWrapper } = require('../helpers');
const { noteExist } = require('../middlewares/noteExistMiddleware');
const { addNote, getAllNotes, getNoteById, updateNote, changeStatus, deleteNote } = require('../controllers/notesController');

router.post('/', asyncWrapper(addNote));
router.get('/', asyncWrapper(getAllNotes));
router.get('/:id', noteExist(Note), asyncWrapper(getNoteById));
router.put('/:id', noteExist(Note), asyncWrapper(updateNote));
router.patch('/:id', noteExist(Note), asyncWrapper(changeStatus));
router.delete('/:id', noteExist(Note), asyncWrapper(deleteNote));

module.exports = router;