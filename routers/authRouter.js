const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('../helpers');
const { login, register } = require('../controllers/authController');
const { validateLogin, validateRegister } = require('../middlewares/validationMiddleware');

router.post('/login', asyncWrapper(validateLogin), asyncWrapper(login));
router.post('/register', asyncWrapper(validateRegister), asyncWrapper(register));

module.exports = router;