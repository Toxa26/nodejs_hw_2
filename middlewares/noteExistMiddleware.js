module.exports.noteExist = model => async (req, res, next) => {
  const { id } = req.params;

  if (!(await model.exists({_id: id}))) {
    return res.status(400).json({message: 'No note with this id'});
  }

  next();
};
