const Joi = require('joi');

const options = {
  errors: {
    wrap: {
      label: `'`
    }
  }
};

const validateRegister = async (req, res, next) => {
    const schema = Joi.object({
        username: Joi.string()
            .alphanum()
            .min(3)
            .max(10)
            .required(),
    
        password: Joi.string()
            .required()
            .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    });

    await schema.validateAsync(req.body, options);
    next();
}

const validateLogin = async (req, res, next) => {
  const schema = Joi.object({
      username: Joi.string()
          .required(),
  
      password: Joi.string()
          .required(),
  });

  await schema.validateAsync(req.body, options);
  next();
}

module.exports = {
  validateLogin,
  validateRegister
}