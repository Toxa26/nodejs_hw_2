const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

require('dotenv').config();

const PORT = process.env.PORT || 8080;

const { authMiddleware } = require('./middlewares/authMiddleware');

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const notesRouter = require('./routers/notesRouter');

const { BadRequest } = require('./errors/badRequest');
const { UnauthorizedUser } = require('./errors/unauthorizedUser');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, usersRouter);
app.use('/api/notes', authMiddleware, notesRouter);

app.use((err, req, res, next) => {
  if (err instanceof BadRequest) {
      res.status(err.statusCode).json({message: err.message});
  }

  if (err instanceof UnauthorizedUser) {
      res.status(err.statusCode).json({message: err.message});
  }

  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://admin:adminpass@cluster0.qnl51.mongodb.net/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
  });

  app.listen(PORT, () => {
    console.log(`Server has been worked on ${PORT}...`);
  });
};

start();