const { Note } = require('../models/noteModel');

const addNote = async (req, res) => {
  const { text } = req.body;

  if (!text) {
    return res.status(400).json({message: 'Enter text field!'});
  }

  const note = new Note({
    userId: req.user._id,
    text
  });

  await note.save();
  res.status(200).json({message: 'Note added successfully'});
};

const getAllNotes = async (req, res) => {
  const { offset = 0, limit = 5 } = req.query;
  const notes = await Note.find(
    { userId: req.user._id },
    ['userId', 'text', 'createdDate', 'completed'], {
      skip: +offset,
      limit: +limit,
      sort: {
        createdDate: -1,
      }
    }
  );

  res.status(200).json({notes});
};

const getNoteById = async (req, res) => {
  const { id } = req.params;
  const currentNote = await Note.findById(id, {_v: 0}).exec();

  res.status(200).json({
    note: {
      _id: currentNote._id,
      userId: currentNote.userId,
      completed: currentNote.completed,
      text: currentNote.text,
      createdDate: currentNote.createdDate
    }
  });
};

const updateNote = async (req, res) => {
  const { id } = req.params;
  const { text } = req.body;
  const currentNote = await Note.findById(id, {_v: 0}).exec();

  currentNote.text = text;
  await currentNote.save();
  res.status(200).json({message: 'Success'});
};

const changeStatus = async (req, res) => {
  const { id } = req.params;
  const currentNote = await Note.findById(id, {_v: 0}).exec();

  currentNote.completed = !currentNote.completed;
  await currentNote.save();
  res.status(200).json({message: 'Success'});
};

const deleteNote = async (req, res) => {
  const { id } = req.params;

  await Note.findByIdAndRemove(id);
  res.status(200).json({message: 'Success'});
};

module.exports = {
  addNote,
  getAllNotes,
  getNoteById,
  updateNote,
  changeStatus,
  deleteNote
}