const bcrypt = require('bcrypt');
const { User } = require('../models/userModel');

const getProfile = async (req, res) => {
  const user = await User.findById(req.user._id, 'username createdDate');

  if (!user) {
    return res.status(400).json({message: `No user was found!`});
  }

  res.status(200).json({user: { _id: user._id, username: user.username, createdDate: user.createdDate }});
}

const removeUser = async (req, res) => {
  await User.findByIdAndRemove(req.user._id);

  res.status(200).json({message: 'Success'});
}

const changePass = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const user = await User.findById(req.user._id, 'password').exec();

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'Wrong old password!'});
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();

  res.status(200).json({message: 'Password successfuly changed!'});
}

module.exports = {
  getProfile,
  removeUser,
  changePass
}